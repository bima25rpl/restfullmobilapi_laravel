<?php

namespace App\Http\Controllers;

use App\Mobil;
use Illuminate\Http\Request;

class MobilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $mobil = Mobil::all();
        return response()->json(['data' => $mobil]);
        //
    }

    public function merek($merek)
    {
         $mobil = Mobil::where('merek',$merek)->first();
        return response()->json(['data' => $mobil]);
        //
    }

    public function type($merek, $tipe)
    {
         $mobil = Mobil::where(['merek' => $merek , 'tipe' => $tipe])->first();
        return response()->json(['data' => $mobil]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        $mobil = new Mobil;
        $mobil->NomorKendaraan = $request->NomorKendaraan;
        $mobil->NomorPolisi = $request->NomorPolisi;
        $mobil->merek = $request->merek;
        $mobil->tipe = $request->tipe;
        $mobil->tahun = $request->tahun;
        $mobil->save();

        return "data berhasil masuk";
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mobil  $mobil
     * @return \Illuminate\Http\Response
     */
    public function show(Mobil $mobil)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mobil  $mobil
     * @return \Illuminate\Http\Response
     */
    public function edit(Mobil $mobil)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mobil  $mobil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Mobil $NomorKendaraan)
    {
        $mobil = Mobil::find($NomorKendaraan)->first();     
        $mobil->NomorKendaraan = $request->NomorKendaraan;
        $mobil->NomorPolisi = $request->NomorPolisi;
        $mobil->merek = $request->merek;
        $mobil->tipe = $request->tipe;
        $mobil->tahun = $request->tahun;
        $mobil->update();

        return "Data berhasil di update";
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mobil  $mobil
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mobil $NomorKendaraan)
    {
        $mobil = Mobil::find($NomorKendaraan)->first();     
        $mobil->delete();

        return "data Berhasil di hapus";
        //
    }

}
