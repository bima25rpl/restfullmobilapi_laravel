<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::Get('mobil','MobilController@index');
Route::Get('/mobil/{merek}','MobilController@merek');
Route::Get('/mobil/{merek}/{tipe}','MobilController@type');
Route::post('mobil','MobilController@create');
Route::put('/mobil/{NomorKendaraan}','MobilController@update');
Route::delete('/mobil/{NomorKendaraan}','MobilController@destroy');

