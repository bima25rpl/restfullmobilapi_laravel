					@extends('home')
					@section('konten')
					<div class="row" id="cobaSaja">
						<div class="col-md-4">
							<!-- MULTI CHARTS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Tambah Data</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body">
									<form action="{{route('mobil.store')}}" method="POST">
									{{ csrf_field() }}
									<input type="number" class="form-control" placeholder="Nomor Kedaraan" name="NomorKendaraan" *required>
									<br>
									<input type="text" class="form-control" placeholder="Nomor Polisi" name="NomorPolisi" *required>
									<br>
									<input type="text" class="form-control" placeholder="Merek" name="merek" >
									<br>
									<input type="text" class="form-control" placeholder="Type" name="tipe" >
									<br>
									<input type="year" class="form-control" placeholder="Tahun" name="tahun" >
									<br>

									<input type="submit" class="btn btn-primary" name="submit" value="Tambah">
									</form>
								</div>
							</div>
							<!-- END MULTI CHARTS -->
						</div>
						<div class="col-md-8">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Data Mobil</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
									<table class="table table-hover table-striped ">
										<thead>
											<tr>
												<th>Order No.</th>
												<th>Name</th>
												<th>Amount</th>
												<th>Date &amp; Time</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody id="listKendaraan">
											<tr>

											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					@endsection

					@section('script')
					<script>
						var listDataMobil = []
						var elTarget = document.getElementById("listKendaraan")
						var dataIndex = 0
						var dataRender = ""
						function loadData(item, dataIndex){
							// dataRender += "<tr> <td>" + item[dataIndex].NomorKendaraan + "</td> <td>" + item[dataIndex].NomorPolisi + "</td> <td>" + item[dataIndex].merek + "</td> <td>" + item[dataIndex].tipe + "</td> <td>" + item[dataIndex].tahun + "</td> <td><button class='btn btn-danger'>Delete</button></td> </tr>"
							dataRender += `
								<tr>
									<td>
										` + item[dataIndex].NomorKendaraan + `
									</td>

									
									<td>
										` + item[dataIndex].NomorPolisi + `
									</td>

									
									<td>
										` + item[dataIndex].merek + `
									</td>

									
									<td>
										` + item[dataIndex].tipe + `
									</td>

									
									<td>
										` + item[dataIndex].tahun + `
									</td>
									<td>
									 	<form action="{{route('mobil.destroy', '`+item[dataIndex].$NomorKendaraan+`')}}" method="post">
											{{ csrf_field() }}
											{{ method_field("DELETE")}}
											<button class="btn btn-danger" type="submit">Delete</button>
										</form>
									</td>
								</tr>
							`
						}

						$(document).ready(() => {
							$.ajax({
								type:"GET",
								dataType:"json",
								url:"http://localhost:8000/api/mobil/",
								success:function(data){
									listDataMobil = data.data
									console.log(listDataMobil.length)
									for(dataIndex = 0; dataIndex < listDataMobil.length; dataIndex++){
										loadData(listDataMobil, dataIndex)
										console.log("1")
									}
									
									elTarget.innerHTML = dataRender
								}
							})
						})
					</script>
					@endsection